import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        BinarySearch binarySearch = new BinarySearch();
        System.out.println("Please enter the number of test case");
        int timeToInsert = input.nextInt();
        for (int i = 0; i < timeToInsert; i++) {
            System.out.println("Please enter the value to insert in BST");
            int value = input.nextInt();
            if (binarySearch.accept(value)) {
                System.out.printf("The value %d added%n", value);
            } else {
                System.out.printf("The value %d already exists%n", value);
            }
        }
        System.out.println("\nmax depth is: ");
        System.out.println(binarySearch.maxDepth());
        System.out.println("enter value to find depth");
        int value = input.nextInt();
        System.out.printf("depth of %d%n", value);
        System.out.println(binarySearch.findDepth(value));
    }
}
