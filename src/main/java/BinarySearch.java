public class BinarySearch {
    private Node root;

    public boolean accept(int value) {
        if (root == null) {
            root = new Node(value);
            return true;
        } else {
            return root.accept(value);
        }

    }

    public int maxDepth() {
        return maxDepth(root);
    }

    public int maxDepth(Node node) {
        if (node == null)
            return -1;
        else {
            int leftDepth = maxDepth(node.getLeft());
            int rightDepth = maxDepth(node.getRight());

            if (leftDepth > rightDepth)
                return (leftDepth + 1);
            else
                return (rightDepth + 1);
        }
    }

    public int findDepth(int value) {
        return findDepth(root, value);
    }

    public int findDepth(Node node, int value) {
        if (node == null) {
            return -1;
        }
        int depth = -1;
        if (node.getData() == value)
            depth = 0;
        else if (node.getData() > value && findDepth(node.getLeft(), value) >= 0) {
            depth = findDepth(node.getLeft(), value);
            return depth + 1;
        } else if (node.getData() < value && findDepth(node.getRight(), value) >= 0) {
            depth = findDepth(node.getRight(), value);
            return depth + 1;
        }
        return depth;
    }


}

