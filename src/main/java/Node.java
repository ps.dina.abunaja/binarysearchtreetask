public class Node {
    private final int data;
    private Node left;
    private Node right;

    public Node(int data) {
        this.data = data;
        left = right = null;
    }

    public boolean accept(int value) {
        if (value == data) {
            return false;
        } else if (value > data) {
            if (right == null) {
                right = new Node(value);
                return true;
            } else {
                return right.accept(value);
            }

        } else {
            if (left == null) {
                left = new Node(value);
                return true;
            } else {
                return left.accept(value);
            }

        }
    }

    public int getData() {
        return data;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }
}
